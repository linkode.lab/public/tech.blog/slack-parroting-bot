# オウム返し Bot

2019年12月6日の技術ブログのネタで実際に作成した Bot です。
Slack で メンション付きのメッセージを流すと、単純にオウム返ししてくれるだけの Bot です。
Quarkus を使って native-image にしたものを AWS-Lambda で動かします。

[当該ブログの記事](https://blog.linkode.co.jp/entry/2019/12/06/000000)

## 構成

- README.md：この文書です
- *.sh：ビルド、動作確認およびデプロイの際に利用するシェルスクリプトです。
- payload.json：動作確認の際に利用します。input の情報を記載します。
- pom.xml：Maven のビルドの設定ファイルです。
- src：ソースファイルです。
  
## 必要要件

ビルドの際、ローカルマシンには以下のものが必要です

- Linux または Docker
- GraalVM（Linux版）のライブラリ
- Maven
- JDK8
- AWS CLI
    - デプロイを行うアカウントの IAM 設定を済ませておきます
 
## ビルドと AWS Lambda へのデプロイ
 
- リポジトリをローカルに clone します
```shell script
$ git clone https://gitlab.com/linkode.lab/inhouse/slackparrotingbot.git
$ cd slackparrotingbot
```

- ビルドを行います
    - Linux 環境でビルドする場合
        ```shell script
        $ sh build_on_linux.sh
        ```
    - 非Linux環境でビルドする場合、Docker を起動した状態で以下を実行
        ```shell script
        $ sh build_on_non_linux.sh
        ``` 
- `create-native.sh` の IAM ロールの箇所を書き換えます
    - Lambda に適切な権限を付与してください。
- ビルドが完了したら、デプロイします
    ```shell script
    $ sh create-native.sh
    $ sh update-native.sh
    ```
- Lambda の環境変数を設定します
    - `ACCESS_TOKEN`：Slack の「Bot User OAuth Access Token」
    - `CHANNEL`：投稿先のチャンネル名

## Slack の設定

- AWS Lambda へのデプロイが完了したら、Lambda に紐づく API Gateway の設定を行います
    - POST メソッドの設定のみで OK
- API Gateway のエンドポイントの URL を Slack の Bot アプリに設定します

## テスト
 
- `payload.json` の内容を書き換えます。
    - Slack の URL 認証をテストする場合の JSON
        ```json
         {
             "token": "Jhj5dZrVaK7ZwHHjRyZWjbDl",
             "challenge": "3eZbrw1aBm2rZgRNFdxV2595E9CY3gmdALWMmHkvFXO7tYXAYM8P",
             "type": "url_verification"
         }
        ```
    - Bot の動作確認をする場合の JSON
        ```json
        {
          "token": "ZZZZZZWSxiZZZ2yIvs3peJ",
          "team_id": "T061EG9R6",
          "api_app_id": "A0MDYCDME",
          "event": {
            "type": "app_mention",
            "user": "U061F7AUR",
            "text": "What ever happened to <@U0LAN0Z89>?",
            "ts": "1515449438.000011",
            "channel": "C0LAN2Q65",
            "event_ts": "1515449438000011"
          },
          "type": "event_callback",
          "event_id": "Ev0MDYGDKJ",
          "event_time": 1515449438000011,
          "authed_users": [
            "U0LAN0Z89"
          ]
        }
        ```
- 次のシェルスクリプトを実行して、動作確認ができます。
    ```shell script
    $ sh invoke-native.sh
    ```
 
## その他

- Lambda のリクエストハンドラを URL 認証用にするか本番用にするかの切り替えは、`src/main/resources/application.properties` の `quarkus.lambda.handler` の値を変えることで切り替えられます。
    - "test" にした場合：URL 認証用
    - "main" にした場合：本番用
 