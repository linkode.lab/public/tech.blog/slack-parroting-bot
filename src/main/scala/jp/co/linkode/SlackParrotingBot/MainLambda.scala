package jp.co.linkode.SlackParrotingBot

import com.amazonaws.services.lambda.runtime.{Context, RequestHandler}
import javax.inject.Named
import jp.co.linkode.SlackParrotingBot.models.{MessageChannelsEvent, OutputForMain}
import scalaj.http.Http

@Named("main")
class MainLambda extends RequestHandler[MessageChannelsEvent, OutputForMain] {

  val ACCESS_TOKEN = "ACCESS_TOKEN"
  val CHANNEL = "CHANNEL"

  override def handleRequest(input: MessageChannelsEvent, context: Context): OutputForMain = {
    val message = input.event.text.replaceFirst(s"<@[a-zA-Z0-9]+>\\s*", "")
    parrot(message)
  }

  def parrot(message: String): OutputForMain = {
    val text = s"『${message}』"
    val sendData = s"token=${System.getenv(ACCESS_TOKEN)}&channel=${System.getenv(CHANNEL)}&text=${text}"
    val request = Http("https://slack.com/api/chat.postMessage").postData(sendData).method("POST")
    request.execute()
    OutputForMain("OK", "オウム返し成功")
  }
}