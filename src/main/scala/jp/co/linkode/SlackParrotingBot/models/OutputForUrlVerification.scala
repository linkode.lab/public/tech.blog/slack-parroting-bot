package jp.co.linkode.SlackParrotingBot.models

import scala.beans.BeanProperty

case class OutputForUrlVerification(@BeanProperty var challenge: String) {
  def this() = this("")
}
