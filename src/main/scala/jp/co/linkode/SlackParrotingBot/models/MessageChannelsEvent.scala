package jp.co.linkode.SlackParrotingBot.models

import scala.beans.BeanProperty

case class MessageChannelsEvent(@BeanProperty var token: String,
                                @BeanProperty var teamId: String,
                                @BeanProperty var apiAppId: String,
                                @BeanProperty var event: MessageChannelsEventType,
                                @BeanProperty var `type`: String,
                                @BeanProperty var authedUsers: List[String],
                                @BeanProperty var eventId: String,
                                @BeanProperty var eventTime: BigInt) {
  def this() = this("", "", "", new MessageChannelsEventType(), "", List(""), "", BigInt(0))
}
