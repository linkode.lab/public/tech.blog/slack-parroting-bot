package jp.co.linkode.SlackParrotingBot.models

import scala.beans.BeanProperty

case class OutputForMain(@BeanProperty var statusCode: String,
                         @BeanProperty var message: String) {
  def this() = this("", "")
}
