package jp.co.linkode.SlackParrotingBot.models

import scala.beans.BeanProperty

case class UrlVerificationEvent(@BeanProperty var token: String,
                                @BeanProperty var challenge: String,
                                @BeanProperty var eventType: String) {

  def this() = this("", "", "")
}
