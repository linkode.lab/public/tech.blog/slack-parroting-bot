package jp.co.linkode.SlackParrotingBot.models

import scala.beans.BeanProperty

case class MessageChannelsEventType(@BeanProperty var `type`: String,
                                    @BeanProperty var subtype: String,
                                    @BeanProperty var channel: String,
                                    @BeanProperty var user: String,
                                    @BeanProperty var botId: String,
                                    @BeanProperty var text: String,
                                    @BeanProperty var ts: String,
                                    @BeanProperty var eventTs: String,
                                    @BeanProperty var channelType: String) {
  def this() = this("", "", "", "", "", "", "", "", "")
}
