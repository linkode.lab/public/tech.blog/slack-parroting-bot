import com.amazonaws.services.lambda.runtime.{Context, RequestHandler}
import jp.co.linkode.SlackParrotingBot.models.{OutputForUrlVerification, UrlVerificationEvent}
import javax.inject.Named

@Named("test")
class TestUrlVerificationLambda extends RequestHandler[UrlVerificationEvent, OutputForUrlVerification] {
  override def handleRequest(input: UrlVerificationEvent, context: Context): OutputForUrlVerification = {
    OutputForUrlVerification(input.challenge)
  }
}